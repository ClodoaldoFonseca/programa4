from math import gamma
from decimal import Decimal
import math,csv


def calcFun(dof, x):
    saida = math.gamma((dof+1)/2)/math.pow(dof*math.pi,1/2)*math.gamma(dof/2)*math.pow(1+(x**2/2),-dof+1/2)
    return saida

def juntando(w,multiplicador,fx):
    return ((w/3)*multiplicador*fx)


def calculaResultado(x,w,dof,resultadoEsperado,num_seg,multiplicador):
    i =0
    ant =0
    while(i < num_seg):
        if(i==0):
            w=0
            pt1 = calcFun(dof,x*i)
            fim = juntando(x*i,multiplicador,pt1)
        else:
            pt1 = calcFun(dof,x*i)
            fim = juntando(x*i,multiplicador,pt1)
            ant = fim
        i=i+1
    return fim

def readCsv():
    lista = list()
    with open('teste.csv','r') as csvfile:
        reader= csv.reader(csvfile)
        for row in reader:
            lista.append(row)
    return lista

def main():
    valores = readCsv()
    e = 0.00001
    num_seg = 10

    for val in valores:
        val = val[0].split(';')
        x=float(val[0])
        w = x/num_seg
        multiplicador=2
        dof=float(val[1])
        resultadoEsperado = float(val[2])
        resultadoFinal = calculaResultado(x,w,dof,resultadoEsperado,num_seg,multiplicador)
        print(resultadoFinal)
main()
