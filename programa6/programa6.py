from math import gamma
from decimal import Decimal
import math,csv

def calculaResultado(x,w,dof,resultadoEsperado,num_seg,multiplicador):
    i =0
    ant =0
    while(i < num_seg):
        if(i==0):
            w=0
            pt1 = calcFun(dof,x*i)
            fim = juntando(x*i,multiplicador,pt1)
        else:
            pt1 = calcFun(dof,x*i)
            fim = juntando(x*i,multiplicador,pt1)
            ant = fim
        i=i+1
    return fim

def readCsv():
    lista = list()
    with open('teste.csv','r') as csvfile:
        reader= csv.reader(csvfile)
        for row in reader:
            lista.append(row)
    return lista
def main():
    valores = readCsv()
    print(valores)

main()
